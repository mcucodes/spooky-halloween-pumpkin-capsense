/*****************************************************************************
* File Name: main.c
*
* Version: 1.0
*
* Description:
* This is the top level application file.
*
* Hardware Dependency:
* CY8CKIT-042 BLE Pioneer Kit
*
******************************************************************************
* Copyright (2014), Cypress Semiconductor Corporation.
******************************************************************************
* This software is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and
* foreign), United States copyright laws and international treaty provisions.
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the
* Cypress Source Code and derivative works for the sole purpose of creating
* custom software in support of licensee product to be used only in conjunction
* with a Cypress integrated circuit as specified in the applicable agreement.
* Any reproduction, modification, translation, compilation, or representation of
* this software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH
* REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising out
* of the application or use of any product or circuit described herein. Cypress
* does not authorize its products for use as critical components in life-support
* systems where a malfunction or failure may reasonably be expected to result in
* significant injury to the user. The inclusion of Cypress' product in a life-
* support systems application implies that the manufacturer assumes all risk of
* such use and in doing so indemnifies Cypress against all charges. Use may be
* limited by and subject to the applicable Cypress software license agreement.
*****************************************************************************/


/*****************************************************************************
* Included headers
*****************************************************************************/
#include <main.h>
#include <BLEApplications.h>
#include <dfplay.h>
#include <stdlib.h>

/*****************************************************************************
* Function Prototypes
*****************************************************************************/
static void InitializeSystem(void);
static void HandleCapSense(void);

volatile uint8 rnum =0;
volatile uint32_t tickStatus,tickCount;
volatile uint16_t blink_value=100;

/*****************************************************************************
* Public functions
*****************************************************************************/

void SysTickISRCallback(void)
{
    tickCount++;

}

/*******************************************************************************
* Function Name: main
********************************************************************************
* Summary:
* System entrance point. This calls the initializing function and continuously
* process BLE and CapSense events.
*
* Parameters:
*  void
*
* Return:
*  int
*
*******************************************************************************/
int main()
{
    uint16_t i;
	/* This function will initialize the system resources such as BLE and CapSense */
    InitializeSystem();
    CySysTickStart();

    /* Find unused callback slot and assign the callback. */
    for (i = 0u; i < CY_SYS_SYST_NUM_OF_CALLBACKS; ++i)
    {
        if (CySysTickGetCallback(i) == NULL)
        {
            /* Set callback */
            CySysTickSetCallback(i, SysTickISRCallback);
            break;
        }
    }
    for(;;)
    {
        /*Process event callback to handle BLE events. The events generated and 
		* used for this application are inside the 'CustomEventHandler' routine*/
        CyBle_ProcessEvents();
		
        if(deviceConnected==1u){
            
            
        if(TRUE == sendCapSenseSliderNotifications)
		{
            
       /* Check for CapSense slider swipe and send data accordingly */
		HandleCapSense();
        
                /* Tick status fires only once every millisecond. */
        if(tickCount>blink_value  && (!DFBusy_Read()))
        {
            /* Reset tick status. */
                tickCount=0;
                tickStatus=1-tickStatus;
                 LED2_Write(~LED2_Read());
                 SendCapSenseNotification((uint8)90*tickStatus);
        }

        if(DFBusy_Read()){
             LED2_Write(1u);
             SendCapSenseNotification((uint8)0);
        } 
        

        }
    
        
        }
        
    }	
}


/*******************************************************************************
* Function Name: InitializeSystem
********************************************************************************
* Summary:
* Start the components and initialize system.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void InitializeSystem(void)
{
	/* Enable global interrupt mask */
	CyGlobalIntEnable; 
	
	/* Start BLE component and register the CustomEventHandler function. This 
	 * function exposes the events from BLE component for application use */
    CyBle_Start(CustomEventHandler);	
    
	/* Start both the PrISM components for LED control*/
    PRS_1_Start();
    PRS_2_Start();

	/* The RGB LED on BLE Pioneer kit are active low. Drive HIGH on 
	 * pin for OFF and drive LOW on pin for ON*/
	PRS_1_WritePulse0(RGB_LED_OFF);
	PRS_1_WritePulse1(RGB_LED_OFF);
	PRS_2_WritePulse0(RGB_LED_OFF);
	
	/* Set Drive mode of output pins from HiZ to Strong */
	RED_SetDriveMode(RED_DM_STRONG);
	GREEN_SetDriveMode(GREEN_DM_STRONG);
	BLUE_SetDriveMode(BLUE_DM_STRONG);
	
    CapSense_EnableWidget(CapSense_PROXIMITYSENSOR0__PROX);

    
    CapSense_Start();

    /* Initialize baselines */
    CapSense_InitializeSensorBaseline(CapSense_PROXIMITYSENSOR0__PROX);
        
    DF_UART_Start();

    DF_DeviceVolume(30);

}


/*******************************************************************************
* Function Name: HandleCapSenseSlider
********************************************************************************
* Summary:
* This function scans for finger position on CapSense slider, and if the  
* position is different, triggers separate routine for BLE notification.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void HandleCapSense(void)
{
	/* Last read CapSense slider position value */
	static uint16 lastPosition;	
	
	/* Present slider position read by CapSense */
	uint16 sliderPosition;
		

	
	/* Wait for CapSense scanning to be complete. This could take about 5 ms */
	//while(CapSense_IsBusy());
	
    if(CapSense_IsBusy() == 0)
    { 
         /* Update proximity sensor baseline */
        CapSense_UpdateSensorBaseline(CapSense_PROXIMITYSENSOR0__PROX);
  
        /* Check if proximity sensor is active - thresholds are dynamically calculated during run-time
            proximityActive variable will have a non-zero value if Proximity was active */
          uint8_t value1  = CapSense_CheckIsSensorActive(CapSense_PROXIMITYSENSOR0__PROX);
      
        /* Process output if proximity sensor was active */
        LED_Write(~value1);

        if(value1 !=0  && (DFBusy_Read())){

            
            DF_PlaySel(1,rnum);
            rnum = rand() %10;    
            blink_value = (rand()%100) + 100;
        }

        // LED_Write(~value1);         
        /* Trigger the next Scan of proximity sensor */
        CapSense_ScanSensor(CapSense_PROXIMITYSENSOR0__PROX);

    }	
}

/* [] END OF FILE */
