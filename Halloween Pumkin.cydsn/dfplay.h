/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <project.h>

#define DF_NEXT 0x01

#define DF_PREV 0x02
#define DF_PLAY 0x03
#define DF_INVOL 0x04
#define DF_DEVOL 0x05
#define DF_SEVOL 0x06

#define DF_SEDEV 0x09
#define DF_RESET 0x0C
#define DF_PLYBA 0x0D
#define DF_PAUSE 0x0E
#define DF_SEPLY 0x0F


void DF_Play(uint16 track);
void DF_DeviceReset();
void DF_DeviceSel(uint8 device);
void DF_PlaySel(uint8 folder,uint8 track);
void DF_DeviceVolume(uint8 level);

/* [] END OF FILE */
