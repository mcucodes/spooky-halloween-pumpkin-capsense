/*******************************************************************************
* File Name: DFBusy.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_DFBusy_H) /* Pins DFBusy_H */
#define CY_PINS_DFBusy_H

#include "cytypes.h"
#include "cyfitter.h"
#include "DFBusy_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} DFBusy_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   DFBusy_Read(void);
void    DFBusy_Write(uint8 value);
uint8   DFBusy_ReadDataReg(void);
#if defined(DFBusy__PC) || (CY_PSOC4_4200L) 
    void    DFBusy_SetDriveMode(uint8 mode);
#endif
void    DFBusy_SetInterruptMode(uint16 position, uint16 mode);
uint8   DFBusy_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void DFBusy_Sleep(void); 
void DFBusy_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(DFBusy__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define DFBusy_DRIVE_MODE_BITS        (3)
    #define DFBusy_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - DFBusy_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the DFBusy_SetDriveMode() function.
         *  @{
         */
        #define DFBusy_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define DFBusy_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define DFBusy_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define DFBusy_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define DFBusy_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define DFBusy_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define DFBusy_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define DFBusy_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define DFBusy_MASK               DFBusy__MASK
#define DFBusy_SHIFT              DFBusy__SHIFT
#define DFBusy_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in DFBusy_SetInterruptMode() function.
     *  @{
     */
        #define DFBusy_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define DFBusy_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define DFBusy_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define DFBusy_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(DFBusy__SIO)
    #define DFBusy_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(DFBusy__PC) && (CY_PSOC4_4200L)
    #define DFBusy_USBIO_ENABLE               ((uint32)0x80000000u)
    #define DFBusy_USBIO_DISABLE              ((uint32)(~DFBusy_USBIO_ENABLE))
    #define DFBusy_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define DFBusy_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define DFBusy_USBIO_ENTER_SLEEP          ((uint32)((1u << DFBusy_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << DFBusy_USBIO_SUSPEND_DEL_SHIFT)))
    #define DFBusy_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << DFBusy_USBIO_SUSPEND_SHIFT)))
    #define DFBusy_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << DFBusy_USBIO_SUSPEND_DEL_SHIFT)))
    #define DFBusy_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(DFBusy__PC)
    /* Port Configuration */
    #define DFBusy_PC                 (* (reg32 *) DFBusy__PC)
#endif
/* Pin State */
#define DFBusy_PS                     (* (reg32 *) DFBusy__PS)
/* Data Register */
#define DFBusy_DR                     (* (reg32 *) DFBusy__DR)
/* Input Buffer Disable Override */
#define DFBusy_INP_DIS                (* (reg32 *) DFBusy__PC2)

/* Interrupt configuration Registers */
#define DFBusy_INTCFG                 (* (reg32 *) DFBusy__INTCFG)
#define DFBusy_INTSTAT                (* (reg32 *) DFBusy__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define DFBusy_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(DFBusy__SIO)
    #define DFBusy_SIO_REG            (* (reg32 *) DFBusy__SIO)
#endif /* (DFBusy__SIO_CFG) */

/* USBIO registers */
#if !defined(DFBusy__PC) && (CY_PSOC4_4200L)
    #define DFBusy_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define DFBusy_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define DFBusy_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define DFBusy_DRIVE_MODE_SHIFT       (0x00u)
#define DFBusy_DRIVE_MODE_MASK        (0x07u << DFBusy_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins DFBusy_H */


/* [] END OF FILE */
