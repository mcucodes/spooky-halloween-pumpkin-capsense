/***************************************************************************//**
* \file DF_UART_SPI_UART_INT.c
* \version 4.0
*
* \brief
*  This file provides the source code to the Interrupt Service Routine for
*  the SCB Component in SPI and UART modes.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "DF_UART_PVT.h"
#include "DF_UART_SPI_UART_PVT.h"


#if (DF_UART_SCB_IRQ_INTERNAL)
/*******************************************************************************
* Function Name: DF_UART_SPI_UART_ISR
****************************************************************************//**
*
*  Handles the Interrupt Service Routine for the SCB SPI or UART modes.
*
*******************************************************************************/
CY_ISR(DF_UART_SPI_UART_ISR)
{
#if (DF_UART_INTERNAL_RX_SW_BUFFER_CONST)
    uint32 locHead;
#endif /* (DF_UART_INTERNAL_RX_SW_BUFFER_CONST) */

#if (DF_UART_INTERNAL_TX_SW_BUFFER_CONST)
    uint32 locTail;
#endif /* (DF_UART_INTERNAL_TX_SW_BUFFER_CONST) */

#ifdef DF_UART_SPI_UART_ISR_ENTRY_CALLBACK
    DF_UART_SPI_UART_ISR_EntryCallback();
#endif /* DF_UART_SPI_UART_ISR_ENTRY_CALLBACK */

    if (NULL != DF_UART_customIntrHandler)
    {
        DF_UART_customIntrHandler();
    }

    #if(DF_UART_CHECK_SPI_WAKE_ENABLE)
    {
        /* Clear SPI wakeup source */
        DF_UART_ClearSpiExtClkInterruptSource(DF_UART_INTR_SPI_EC_WAKE_UP);
    }
    #endif

    #if (DF_UART_CHECK_RX_SW_BUFFER)
    {
        if (DF_UART_CHECK_INTR_RX_MASKED(DF_UART_INTR_RX_NOT_EMPTY))
        {
            do
            {
                /* Move local head index */
                locHead = (DF_UART_rxBufferHead + 1u);

                /* Adjust local head index */
                if (DF_UART_INTERNAL_RX_BUFFER_SIZE == locHead)
                {
                    locHead = 0u;
                }

                if (locHead == DF_UART_rxBufferTail)
                {
                    #if (DF_UART_CHECK_UART_RTS_CONTROL_FLOW)
                    {
                        /* There is no space in the software buffer - disable the
                        * RX Not Empty interrupt source. The data elements are
                        * still being received into the RX FIFO until the RTS signal
                        * stops the transmitter. After the data element is read from the
                        * buffer, the RX Not Empty interrupt source is enabled to
                        * move the next data element in the software buffer.
                        */
                        DF_UART_INTR_RX_MASK_REG &= ~DF_UART_INTR_RX_NOT_EMPTY;
                        break;
                    }
                    #else
                    {
                        /* Overflow: through away received data element */
                        (void) DF_UART_RX_FIFO_RD_REG;
                        DF_UART_rxBufferOverflow = (uint8) DF_UART_INTR_RX_OVERFLOW;
                    }
                    #endif
                }
                else
                {
                    /* Store received data */
                    DF_UART_PutWordInRxBuffer(locHead, DF_UART_RX_FIFO_RD_REG);

                    /* Move head index */
                    DF_UART_rxBufferHead = locHead;
                }
            }
            while(0u != DF_UART_GET_RX_FIFO_ENTRIES);

            DF_UART_ClearRxInterruptSource(DF_UART_INTR_RX_NOT_EMPTY);
        }
    }
    #endif


    #if (DF_UART_CHECK_TX_SW_BUFFER)
    {
        if (DF_UART_CHECK_INTR_TX_MASKED(DF_UART_INTR_TX_NOT_FULL))
        {
            do
            {
                /* Check for room in TX software buffer */
                if (DF_UART_txBufferHead != DF_UART_txBufferTail)
                {
                    /* Move local tail index */
                    locTail = (DF_UART_txBufferTail + 1u);

                    /* Adjust local tail index */
                    if (DF_UART_TX_BUFFER_SIZE == locTail)
                    {
                        locTail = 0u;
                    }

                    /* Put data into TX FIFO */
                    DF_UART_TX_FIFO_WR_REG = DF_UART_GetWordFromTxBuffer(locTail);

                    /* Move tail index */
                    DF_UART_txBufferTail = locTail;
                }
                else
                {
                    /* TX software buffer is empty: complete transfer */
                    DF_UART_DISABLE_INTR_TX(DF_UART_INTR_TX_NOT_FULL);
                    break;
                }
            }
            while (DF_UART_SPI_UART_FIFO_SIZE != DF_UART_GET_TX_FIFO_ENTRIES);

            DF_UART_ClearTxInterruptSource(DF_UART_INTR_TX_NOT_FULL);
        }
    }
    #endif

#ifdef DF_UART_SPI_UART_ISR_EXIT_CALLBACK
    DF_UART_SPI_UART_ISR_ExitCallback();
#endif /* DF_UART_SPI_UART_ISR_EXIT_CALLBACK */

}

#endif /* (DF_UART_SCB_IRQ_INTERNAL) */


/* [] END OF FILE */
