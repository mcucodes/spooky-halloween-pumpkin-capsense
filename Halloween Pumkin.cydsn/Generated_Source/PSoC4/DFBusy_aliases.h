/*******************************************************************************
* File Name: DFBusy.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_DFBusy_ALIASES_H) /* Pins DFBusy_ALIASES_H */
#define CY_PINS_DFBusy_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define DFBusy_0			(DFBusy__0__PC)
#define DFBusy_0_PS		(DFBusy__0__PS)
#define DFBusy_0_PC		(DFBusy__0__PC)
#define DFBusy_0_DR		(DFBusy__0__DR)
#define DFBusy_0_SHIFT	(DFBusy__0__SHIFT)
#define DFBusy_0_INTR	((uint16)((uint16)0x0003u << (DFBusy__0__SHIFT*2u)))

#define DFBusy_INTR_ALL	 ((uint16)(DFBusy_0_INTR))


#endif /* End Pins DFBusy_ALIASES_H */


/* [] END OF FILE */
