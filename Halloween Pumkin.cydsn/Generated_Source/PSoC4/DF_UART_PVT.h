/***************************************************************************//**
* \file .h
* \version 4.0
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_DF_UART_H)
#define CY_SCB_PVT_DF_UART_H

#include "DF_UART.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define DF_UART_SetI2CExtClkInterruptMode(interruptMask) DF_UART_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define DF_UART_ClearI2CExtClkInterruptSource(interruptMask) DF_UART_CLEAR_INTR_I2C_EC(interruptMask)
#define DF_UART_GetI2CExtClkInterruptSource()                (DF_UART_INTR_I2C_EC_REG)
#define DF_UART_GetI2CExtClkInterruptMode()                  (DF_UART_INTR_I2C_EC_MASK_REG)
#define DF_UART_GetI2CExtClkInterruptSourceMasked()          (DF_UART_INTR_I2C_EC_MASKED_REG)

#if (!DF_UART_CY_SCBIP_V1)
    /* APIs to service INTR_SPI_EC register */
    #define DF_UART_SetSpiExtClkInterruptMode(interruptMask) \
                                                                DF_UART_WRITE_INTR_SPI_EC_MASK(interruptMask)
    #define DF_UART_ClearSpiExtClkInterruptSource(interruptMask) \
                                                                DF_UART_CLEAR_INTR_SPI_EC(interruptMask)
    #define DF_UART_GetExtSpiClkInterruptSource()                 (DF_UART_INTR_SPI_EC_REG)
    #define DF_UART_GetExtSpiClkInterruptMode()                   (DF_UART_INTR_SPI_EC_MASK_REG)
    #define DF_UART_GetExtSpiClkInterruptSourceMasked()           (DF_UART_INTR_SPI_EC_MASKED_REG)
#endif /* (!DF_UART_CY_SCBIP_V1) */

#if(DF_UART_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void DF_UART_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask);
#endif /* (DF_UART_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Vars with External Linkage
***************************************/

#if (DF_UART_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_DF_UART_CUSTOM_INTR_HANDLER)
    extern cyisraddress DF_UART_customIntrHandler;
#endif /* !defined (CY_REMOVE_DF_UART_CUSTOM_INTR_HANDLER) */
#endif /* (DF_UART_SCB_IRQ_INTERNAL) */

extern DF_UART_BACKUP_STRUCT DF_UART_backup;

#if(DF_UART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    extern uint8 DF_UART_scbMode;
    extern uint8 DF_UART_scbEnableWake;
    extern uint8 DF_UART_scbEnableIntr;

    /* I2C configuration variables */
    extern uint8 DF_UART_mode;
    extern uint8 DF_UART_acceptAddr;

    /* SPI/UART configuration variables */
    extern volatile uint8 * DF_UART_rxBuffer;
    extern uint8   DF_UART_rxDataBits;
    extern uint32  DF_UART_rxBufferSize;

    extern volatile uint8 * DF_UART_txBuffer;
    extern uint8   DF_UART_txDataBits;
    extern uint32  DF_UART_txBufferSize;

    /* EZI2C configuration variables */
    extern uint8 DF_UART_numberOfAddr;
    extern uint8 DF_UART_subAddrSize;
#endif /* (DF_UART_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (! (DF_UART_SCB_MODE_I2C_CONST_CFG || \
        DF_UART_SCB_MODE_EZI2C_CONST_CFG))
    extern uint16 DF_UART_IntrTxMask;
#endif /* (! (DF_UART_SCB_MODE_I2C_CONST_CFG || \
              DF_UART_SCB_MODE_EZI2C_CONST_CFG)) */


/***************************************
*        Conditional Macro
****************************************/

#if(DF_UART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Defines run time operation mode */
    #define DF_UART_SCB_MODE_I2C_RUNTM_CFG     (DF_UART_SCB_MODE_I2C      == DF_UART_scbMode)
    #define DF_UART_SCB_MODE_SPI_RUNTM_CFG     (DF_UART_SCB_MODE_SPI      == DF_UART_scbMode)
    #define DF_UART_SCB_MODE_UART_RUNTM_CFG    (DF_UART_SCB_MODE_UART     == DF_UART_scbMode)
    #define DF_UART_SCB_MODE_EZI2C_RUNTM_CFG   (DF_UART_SCB_MODE_EZI2C    == DF_UART_scbMode)
    #define DF_UART_SCB_MODE_UNCONFIG_RUNTM_CFG \
                                                        (DF_UART_SCB_MODE_UNCONFIG == DF_UART_scbMode)

    /* Defines wakeup enable */
    #define DF_UART_SCB_WAKE_ENABLE_CHECK       (0u != DF_UART_scbEnableWake)
#endif /* (DF_UART_SCB_MODE_UNCONFIG_CONST_CFG) */

/* Defines maximum number of SCB pins */
#if (!DF_UART_CY_SCBIP_V1)
    #define DF_UART_SCB_PINS_NUMBER    (7u)
#else
    #define DF_UART_SCB_PINS_NUMBER    (2u)
#endif /* (!DF_UART_CY_SCBIP_V1) */

#endif /* (CY_SCB_PVT_DF_UART_H) */


/* [] END OF FILE */
