/***************************************************************************//**
* \file DF_UART_PINS.h
* \version 4.0
*
* \brief
*  This file provides constants and parameter values for the pin components
*  buried into SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PINS_DF_UART_H)
#define CY_SCB_PINS_DF_UART_H

#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cytypes.h"


/***************************************
*   Conditional Compilation Parameters
****************************************/

/* Unconfigured pins */
#define DF_UART_REMOVE_RX_WAKE_SDA_MOSI_PIN  (1u)
#define DF_UART_REMOVE_RX_SDA_MOSI_PIN      (1u)
#define DF_UART_REMOVE_TX_SCL_MISO_PIN      (1u)
#define DF_UART_REMOVE_CTS_SCLK_PIN      (1u)
#define DF_UART_REMOVE_RTS_SS0_PIN      (1u)
#define DF_UART_REMOVE_SS1_PIN                 (1u)
#define DF_UART_REMOVE_SS2_PIN                 (1u)
#define DF_UART_REMOVE_SS3_PIN                 (1u)

/* Mode defined pins */
#define DF_UART_REMOVE_I2C_PINS                (1u)
#define DF_UART_REMOVE_SPI_MASTER_PINS         (1u)
#define DF_UART_REMOVE_SPI_MASTER_SCLK_PIN     (1u)
#define DF_UART_REMOVE_SPI_MASTER_MOSI_PIN     (1u)
#define DF_UART_REMOVE_SPI_MASTER_MISO_PIN     (1u)
#define DF_UART_REMOVE_SPI_MASTER_SS0_PIN      (1u)
#define DF_UART_REMOVE_SPI_MASTER_SS1_PIN      (1u)
#define DF_UART_REMOVE_SPI_MASTER_SS2_PIN      (1u)
#define DF_UART_REMOVE_SPI_MASTER_SS3_PIN      (1u)
#define DF_UART_REMOVE_SPI_SLAVE_PINS          (1u)
#define DF_UART_REMOVE_SPI_SLAVE_MOSI_PIN      (1u)
#define DF_UART_REMOVE_SPI_SLAVE_MISO_PIN      (1u)
#define DF_UART_REMOVE_UART_TX_PIN             (0u)
#define DF_UART_REMOVE_UART_RX_TX_PIN          (1u)
#define DF_UART_REMOVE_UART_RX_PIN             (0u)
#define DF_UART_REMOVE_UART_RX_WAKE_PIN        (1u)
#define DF_UART_REMOVE_UART_RTS_PIN            (1u)
#define DF_UART_REMOVE_UART_CTS_PIN            (1u)

/* Unconfigured pins */
#define DF_UART_RX_WAKE_SDA_MOSI_PIN (0u == DF_UART_REMOVE_RX_WAKE_SDA_MOSI_PIN)
#define DF_UART_RX_SDA_MOSI_PIN     (0u == DF_UART_REMOVE_RX_SDA_MOSI_PIN)
#define DF_UART_TX_SCL_MISO_PIN     (0u == DF_UART_REMOVE_TX_SCL_MISO_PIN)
#define DF_UART_CTS_SCLK_PIN     (0u == DF_UART_REMOVE_CTS_SCLK_PIN)
#define DF_UART_RTS_SS0_PIN     (0u == DF_UART_REMOVE_RTS_SS0_PIN)
#define DF_UART_SS1_PIN                (0u == DF_UART_REMOVE_SS1_PIN)
#define DF_UART_SS2_PIN                (0u == DF_UART_REMOVE_SS2_PIN)
#define DF_UART_SS3_PIN                (0u == DF_UART_REMOVE_SS3_PIN)

/* Mode defined pins */
#define DF_UART_I2C_PINS               (0u == DF_UART_REMOVE_I2C_PINS)
#define DF_UART_SPI_MASTER_PINS        (0u == DF_UART_REMOVE_SPI_MASTER_PINS)
#define DF_UART_SPI_MASTER_SCLK_PIN    (0u == DF_UART_REMOVE_SPI_MASTER_SCLK_PIN)
#define DF_UART_SPI_MASTER_MOSI_PIN    (0u == DF_UART_REMOVE_SPI_MASTER_MOSI_PIN)
#define DF_UART_SPI_MASTER_MISO_PIN    (0u == DF_UART_REMOVE_SPI_MASTER_MISO_PIN)
#define DF_UART_SPI_MASTER_SS0_PIN     (0u == DF_UART_REMOVE_SPI_MASTER_SS0_PIN)
#define DF_UART_SPI_MASTER_SS1_PIN     (0u == DF_UART_REMOVE_SPI_MASTER_SS1_PIN)
#define DF_UART_SPI_MASTER_SS2_PIN     (0u == DF_UART_REMOVE_SPI_MASTER_SS2_PIN)
#define DF_UART_SPI_MASTER_SS3_PIN     (0u == DF_UART_REMOVE_SPI_MASTER_SS3_PIN)
#define DF_UART_SPI_SLAVE_PINS         (0u == DF_UART_REMOVE_SPI_SLAVE_PINS)
#define DF_UART_SPI_SLAVE_MOSI_PIN     (0u == DF_UART_REMOVE_SPI_SLAVE_MOSI_PIN)
#define DF_UART_SPI_SLAVE_MISO_PIN     (0u == DF_UART_REMOVE_SPI_SLAVE_MISO_PIN)
#define DF_UART_UART_TX_PIN            (0u == DF_UART_REMOVE_UART_TX_PIN)
#define DF_UART_UART_RX_TX_PIN         (0u == DF_UART_REMOVE_UART_RX_TX_PIN)
#define DF_UART_UART_RX_PIN            (0u == DF_UART_REMOVE_UART_RX_PIN)
#define DF_UART_UART_RX_WAKE_PIN       (0u == DF_UART_REMOVE_UART_RX_WAKE_PIN)
#define DF_UART_UART_RTS_PIN           (0u == DF_UART_REMOVE_UART_RTS_PIN)
#define DF_UART_UART_CTS_PIN           (0u == DF_UART_REMOVE_UART_CTS_PIN)


/***************************************
*             Includes
****************************************/

#if (DF_UART_RX_WAKE_SDA_MOSI_PIN)
    #include "DF_UART_uart_rx_wake_i2c_sda_spi_mosi.h"
#endif /* (DF_UART_RX_SDA_MOSI) */

#if (DF_UART_RX_SDA_MOSI_PIN)
    #include "DF_UART_uart_rx_i2c_sda_spi_mosi.h"
#endif /* (DF_UART_RX_SDA_MOSI) */

#if (DF_UART_TX_SCL_MISO_PIN)
    #include "DF_UART_uart_tx_i2c_scl_spi_miso.h"
#endif /* (DF_UART_TX_SCL_MISO) */

#if (DF_UART_CTS_SCLK_PIN)
    #include "DF_UART_uart_cts_spi_sclk.h"
#endif /* (DF_UART_CTS_SCLK) */

#if (DF_UART_RTS_SS0_PIN)
    #include "DF_UART_uart_rts_spi_ss0.h"
#endif /* (DF_UART_RTS_SS0_PIN) */

#if (DF_UART_SS1_PIN)
    #include "DF_UART_spi_ss1.h"
#endif /* (DF_UART_SS1_PIN) */

#if (DF_UART_SS2_PIN)
    #include "DF_UART_spi_ss2.h"
#endif /* (DF_UART_SS2_PIN) */

#if (DF_UART_SS3_PIN)
    #include "DF_UART_spi_ss3.h"
#endif /* (DF_UART_SS3_PIN) */

#if (DF_UART_I2C_PINS)
    #include "DF_UART_scl.h"
    #include "DF_UART_sda.h"
#endif /* (DF_UART_I2C_PINS) */

#if (DF_UART_SPI_MASTER_PINS)
#if (DF_UART_SPI_MASTER_SCLK_PIN)
    #include "DF_UART_sclk_m.h"
#endif /* (DF_UART_SPI_MASTER_SCLK_PIN) */

#if (DF_UART_SPI_MASTER_MOSI_PIN)
    #include "DF_UART_mosi_m.h"
#endif /* (DF_UART_SPI_MASTER_MOSI_PIN) */

#if (DF_UART_SPI_MASTER_MISO_PIN)
    #include "DF_UART_miso_m.h"
#endif /*(DF_UART_SPI_MASTER_MISO_PIN) */
#endif /* (DF_UART_SPI_MASTER_PINS) */

#if (DF_UART_SPI_SLAVE_PINS)
    #include "DF_UART_sclk_s.h"
    #include "DF_UART_ss_s.h"

#if (DF_UART_SPI_SLAVE_MOSI_PIN)
    #include "DF_UART_mosi_s.h"
#endif /* (DF_UART_SPI_SLAVE_MOSI_PIN) */

#if (DF_UART_SPI_SLAVE_MISO_PIN)
    #include "DF_UART_miso_s.h"
#endif /*(DF_UART_SPI_SLAVE_MISO_PIN) */
#endif /* (DF_UART_SPI_SLAVE_PINS) */

#if (DF_UART_SPI_MASTER_SS0_PIN)
    #include "DF_UART_ss0_m.h"
#endif /* (DF_UART_SPI_MASTER_SS0_PIN) */

#if (DF_UART_SPI_MASTER_SS1_PIN)
    #include "DF_UART_ss1_m.h"
#endif /* (DF_UART_SPI_MASTER_SS1_PIN) */

#if (DF_UART_SPI_MASTER_SS2_PIN)
    #include "DF_UART_ss2_m.h"
#endif /* (DF_UART_SPI_MASTER_SS2_PIN) */

#if (DF_UART_SPI_MASTER_SS3_PIN)
    #include "DF_UART_ss3_m.h"
#endif /* (DF_UART_SPI_MASTER_SS3_PIN) */

#if (DF_UART_UART_TX_PIN)
    #include "DF_UART_tx.h"
#endif /* (DF_UART_UART_TX_PIN) */

#if (DF_UART_UART_RX_TX_PIN)
    #include "DF_UART_rx_tx.h"
#endif /* (DF_UART_UART_RX_TX_PIN) */

#if (DF_UART_UART_RX_PIN)
    #include "DF_UART_rx.h"
#endif /* (DF_UART_UART_RX_PIN) */

#if (DF_UART_UART_RX_WAKE_PIN)
    #include "DF_UART_rx_wake.h"
#endif /* (DF_UART_UART_RX_WAKE_PIN) */

#if (DF_UART_UART_RTS_PIN)
    #include "DF_UART_rts.h"
#endif /* (DF_UART_UART_RTS_PIN) */

#if (DF_UART_UART_CTS_PIN)
    #include "DF_UART_cts.h"
#endif /* (DF_UART_UART_CTS_PIN) */


/***************************************
*              Registers
***************************************/

#if (DF_UART_RX_SDA_MOSI_PIN)
    #define DF_UART_RX_SDA_MOSI_HSIOM_REG   (*(reg32 *) DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM)
    #define DF_UART_RX_SDA_MOSI_HSIOM_PTR   ( (reg32 *) DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM)
    
    #define DF_UART_RX_SDA_MOSI_HSIOM_MASK      (DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM_MASK)
    #define DF_UART_RX_SDA_MOSI_HSIOM_POS       (DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM_SHIFT)
    #define DF_UART_RX_SDA_MOSI_HSIOM_SEL_GPIO  (DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM_GPIO)
    #define DF_UART_RX_SDA_MOSI_HSIOM_SEL_I2C   (DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM_I2C)
    #define DF_UART_RX_SDA_MOSI_HSIOM_SEL_SPI   (DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM_SPI)
    #define DF_UART_RX_SDA_MOSI_HSIOM_SEL_UART  (DF_UART_uart_rx_i2c_sda_spi_mosi__0__HSIOM_UART)
    
#elif (DF_UART_RX_WAKE_SDA_MOSI_PIN)
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG   (*(reg32 *) DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM)
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_PTR   ( (reg32 *) DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM)
    
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_MASK      (DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_MASK)
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_POS       (DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_SHIFT)
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_SEL_GPIO  (DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_GPIO)
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_SEL_I2C   (DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_I2C)
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_SEL_SPI   (DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_SPI)
    #define DF_UART_RX_WAKE_SDA_MOSI_HSIOM_SEL_UART  (DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__HSIOM_UART)    
   
    #define DF_UART_RX_WAKE_SDA_MOSI_INTCFG_REG (*(reg32 *) DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__INTCFG)
    #define DF_UART_RX_WAKE_SDA_MOSI_INTCFG_PTR ( (reg32 *) DF_UART_uart_rx_wake_i2c_sda_spi_mosi__0__INTCFG)
    #define DF_UART_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS  (DF_UART_uart_rx_wake_i2c_sda_spi_mosi__SHIFT)
    #define DF_UART_RX_WAKE_SDA_MOSI_INTCFG_TYPE_MASK ((uint32) DF_UART_INTCFG_TYPE_MASK << \
                                                                           DF_UART_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS)
#else
    /* None of pins DF_UART_RX_SDA_MOSI_PIN or DF_UART_RX_WAKE_SDA_MOSI_PIN present.*/
#endif /* (DF_UART_RX_SDA_MOSI_PIN) */

#if (DF_UART_TX_SCL_MISO_PIN)
    #define DF_UART_TX_SCL_MISO_HSIOM_REG   (*(reg32 *) DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM)
    #define DF_UART_TX_SCL_MISO_HSIOM_PTR   ( (reg32 *) DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM)
    
    #define DF_UART_TX_SCL_MISO_HSIOM_MASK      (DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM_MASK)
    #define DF_UART_TX_SCL_MISO_HSIOM_POS       (DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM_SHIFT)
    #define DF_UART_TX_SCL_MISO_HSIOM_SEL_GPIO  (DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM_GPIO)
    #define DF_UART_TX_SCL_MISO_HSIOM_SEL_I2C   (DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM_I2C)
    #define DF_UART_TX_SCL_MISO_HSIOM_SEL_SPI   (DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM_SPI)
    #define DF_UART_TX_SCL_MISO_HSIOM_SEL_UART  (DF_UART_uart_tx_i2c_scl_spi_miso__0__HSIOM_UART)
#endif /* (DF_UART_TX_SCL_MISO_PIN) */

#if (DF_UART_CTS_SCLK_PIN)
    #define DF_UART_CTS_SCLK_HSIOM_REG   (*(reg32 *) DF_UART_uart_cts_spi_sclk__0__HSIOM)
    #define DF_UART_CTS_SCLK_HSIOM_PTR   ( (reg32 *) DF_UART_uart_cts_spi_sclk__0__HSIOM)
    
    #define DF_UART_CTS_SCLK_HSIOM_MASK      (DF_UART_uart_cts_spi_sclk__0__HSIOM_MASK)
    #define DF_UART_CTS_SCLK_HSIOM_POS       (DF_UART_uart_cts_spi_sclk__0__HSIOM_SHIFT)
    #define DF_UART_CTS_SCLK_HSIOM_SEL_GPIO  (DF_UART_uart_cts_spi_sclk__0__HSIOM_GPIO)
    #define DF_UART_CTS_SCLK_HSIOM_SEL_I2C   (DF_UART_uart_cts_spi_sclk__0__HSIOM_I2C)
    #define DF_UART_CTS_SCLK_HSIOM_SEL_SPI   (DF_UART_uart_cts_spi_sclk__0__HSIOM_SPI)
    #define DF_UART_CTS_SCLK_HSIOM_SEL_UART  (DF_UART_uart_cts_spi_sclk__0__HSIOM_UART)
#endif /* (DF_UART_CTS_SCLK_PIN) */

#if (DF_UART_RTS_SS0_PIN)
    #define DF_UART_RTS_SS0_HSIOM_REG   (*(reg32 *) DF_UART_uart_rts_spi_ss0__0__HSIOM)
    #define DF_UART_RTS_SS0_HSIOM_PTR   ( (reg32 *) DF_UART_uart_rts_spi_ss0__0__HSIOM)
    
    #define DF_UART_RTS_SS0_HSIOM_MASK      (DF_UART_uart_rts_spi_ss0__0__HSIOM_MASK)
    #define DF_UART_RTS_SS0_HSIOM_POS       (DF_UART_uart_rts_spi_ss0__0__HSIOM_SHIFT)
    #define DF_UART_RTS_SS0_HSIOM_SEL_GPIO  (DF_UART_uart_rts_spi_ss0__0__HSIOM_GPIO)
    #define DF_UART_RTS_SS0_HSIOM_SEL_I2C   (DF_UART_uart_rts_spi_ss0__0__HSIOM_I2C)
    #define DF_UART_RTS_SS0_HSIOM_SEL_SPI   (DF_UART_uart_rts_spi_ss0__0__HSIOM_SPI)
#if !(DF_UART_CY_SCBIP_V0 || DF_UART_CY_SCBIP_V1)
    #define DF_UART_RTS_SS0_HSIOM_SEL_UART  (DF_UART_uart_rts_spi_ss0__0__HSIOM_UART)
#endif /* !(DF_UART_CY_SCBIP_V0 || DF_UART_CY_SCBIP_V1) */
#endif /* (DF_UART_RTS_SS0_PIN) */

#if (DF_UART_SS1_PIN)
    #define DF_UART_SS1_HSIOM_REG  (*(reg32 *) DF_UART_spi_ss1__0__HSIOM)
    #define DF_UART_SS1_HSIOM_PTR  ( (reg32 *) DF_UART_spi_ss1__0__HSIOM)
    
    #define DF_UART_SS1_HSIOM_MASK     (DF_UART_spi_ss1__0__HSIOM_MASK)
    #define DF_UART_SS1_HSIOM_POS      (DF_UART_spi_ss1__0__HSIOM_SHIFT)
    #define DF_UART_SS1_HSIOM_SEL_GPIO (DF_UART_spi_ss1__0__HSIOM_GPIO)
    #define DF_UART_SS1_HSIOM_SEL_I2C  (DF_UART_spi_ss1__0__HSIOM_I2C)
    #define DF_UART_SS1_HSIOM_SEL_SPI  (DF_UART_spi_ss1__0__HSIOM_SPI)
#endif /* (DF_UART_SS1_PIN) */

#if (DF_UART_SS2_PIN)
    #define DF_UART_SS2_HSIOM_REG     (*(reg32 *) DF_UART_spi_ss2__0__HSIOM)
    #define DF_UART_SS2_HSIOM_PTR     ( (reg32 *) DF_UART_spi_ss2__0__HSIOM)
    
    #define DF_UART_SS2_HSIOM_MASK     (DF_UART_spi_ss2__0__HSIOM_MASK)
    #define DF_UART_SS2_HSIOM_POS      (DF_UART_spi_ss2__0__HSIOM_SHIFT)
    #define DF_UART_SS2_HSIOM_SEL_GPIO (DF_UART_spi_ss2__0__HSIOM_GPIO)
    #define DF_UART_SS2_HSIOM_SEL_I2C  (DF_UART_spi_ss2__0__HSIOM_I2C)
    #define DF_UART_SS2_HSIOM_SEL_SPI  (DF_UART_spi_ss2__0__HSIOM_SPI)
#endif /* (DF_UART_SS2_PIN) */

#if (DF_UART_SS3_PIN)
    #define DF_UART_SS3_HSIOM_REG     (*(reg32 *) DF_UART_spi_ss3__0__HSIOM)
    #define DF_UART_SS3_HSIOM_PTR     ( (reg32 *) DF_UART_spi_ss3__0__HSIOM)
    
    #define DF_UART_SS3_HSIOM_MASK     (DF_UART_spi_ss3__0__HSIOM_MASK)
    #define DF_UART_SS3_HSIOM_POS      (DF_UART_spi_ss3__0__HSIOM_SHIFT)
    #define DF_UART_SS3_HSIOM_SEL_GPIO (DF_UART_spi_ss3__0__HSIOM_GPIO)
    #define DF_UART_SS3_HSIOM_SEL_I2C  (DF_UART_spi_ss3__0__HSIOM_I2C)
    #define DF_UART_SS3_HSIOM_SEL_SPI  (DF_UART_spi_ss3__0__HSIOM_SPI)
#endif /* (DF_UART_SS3_PIN) */

#if (DF_UART_I2C_PINS)
    #define DF_UART_SCL_HSIOM_REG  (*(reg32 *) DF_UART_scl__0__HSIOM)
    #define DF_UART_SCL_HSIOM_PTR  ( (reg32 *) DF_UART_scl__0__HSIOM)
    
    #define DF_UART_SCL_HSIOM_MASK     (DF_UART_scl__0__HSIOM_MASK)
    #define DF_UART_SCL_HSIOM_POS      (DF_UART_scl__0__HSIOM_SHIFT)
    #define DF_UART_SCL_HSIOM_SEL_GPIO (DF_UART_sda__0__HSIOM_GPIO)
    #define DF_UART_SCL_HSIOM_SEL_I2C  (DF_UART_sda__0__HSIOM_I2C)
    
    #define DF_UART_SDA_HSIOM_REG  (*(reg32 *) DF_UART_sda__0__HSIOM)
    #define DF_UART_SDA_HSIOM_PTR  ( (reg32 *) DF_UART_sda__0__HSIOM)
    
    #define DF_UART_SDA_HSIOM_MASK     (DF_UART_sda__0__HSIOM_MASK)
    #define DF_UART_SDA_HSIOM_POS      (DF_UART_sda__0__HSIOM_SHIFT)
    #define DF_UART_SDA_HSIOM_SEL_GPIO (DF_UART_sda__0__HSIOM_GPIO)
    #define DF_UART_SDA_HSIOM_SEL_I2C  (DF_UART_sda__0__HSIOM_I2C)
#endif /* (DF_UART_I2C_PINS) */

#if (DF_UART_SPI_SLAVE_PINS)
    #define DF_UART_SCLK_S_HSIOM_REG   (*(reg32 *) DF_UART_sclk_s__0__HSIOM)
    #define DF_UART_SCLK_S_HSIOM_PTR   ( (reg32 *) DF_UART_sclk_s__0__HSIOM)
    
    #define DF_UART_SCLK_S_HSIOM_MASK      (DF_UART_sclk_s__0__HSIOM_MASK)
    #define DF_UART_SCLK_S_HSIOM_POS       (DF_UART_sclk_s__0__HSIOM_SHIFT)
    #define DF_UART_SCLK_S_HSIOM_SEL_GPIO  (DF_UART_sclk_s__0__HSIOM_GPIO)
    #define DF_UART_SCLK_S_HSIOM_SEL_SPI   (DF_UART_sclk_s__0__HSIOM_SPI)
    
    #define DF_UART_SS0_S_HSIOM_REG    (*(reg32 *) DF_UART_ss0_s__0__HSIOM)
    #define DF_UART_SS0_S_HSIOM_PTR    ( (reg32 *) DF_UART_ss0_s__0__HSIOM)
    
    #define DF_UART_SS0_S_HSIOM_MASK       (DF_UART_ss0_s__0__HSIOM_MASK)
    #define DF_UART_SS0_S_HSIOM_POS        (DF_UART_ss0_s__0__HSIOM_SHIFT)
    #define DF_UART_SS0_S_HSIOM_SEL_GPIO   (DF_UART_ss0_s__0__HSIOM_GPIO)  
    #define DF_UART_SS0_S_HSIOM_SEL_SPI    (DF_UART_ss0_s__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_SLAVE_PINS) */

#if (DF_UART_SPI_SLAVE_MOSI_PIN)
    #define DF_UART_MOSI_S_HSIOM_REG   (*(reg32 *) DF_UART_mosi_s__0__HSIOM)
    #define DF_UART_MOSI_S_HSIOM_PTR   ( (reg32 *) DF_UART_mosi_s__0__HSIOM)
    
    #define DF_UART_MOSI_S_HSIOM_MASK      (DF_UART_mosi_s__0__HSIOM_MASK)
    #define DF_UART_MOSI_S_HSIOM_POS       (DF_UART_mosi_s__0__HSIOM_SHIFT)
    #define DF_UART_MOSI_S_HSIOM_SEL_GPIO  (DF_UART_mosi_s__0__HSIOM_GPIO)
    #define DF_UART_MOSI_S_HSIOM_SEL_SPI   (DF_UART_mosi_s__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_SLAVE_MOSI_PIN) */

#if (DF_UART_SPI_SLAVE_MISO_PIN)
    #define DF_UART_MISO_S_HSIOM_REG   (*(reg32 *) DF_UART_miso_s__0__HSIOM)
    #define DF_UART_MISO_S_HSIOM_PTR   ( (reg32 *) DF_UART_miso_s__0__HSIOM)
    
    #define DF_UART_MISO_S_HSIOM_MASK      (DF_UART_miso_s__0__HSIOM_MASK)
    #define DF_UART_MISO_S_HSIOM_POS       (DF_UART_miso_s__0__HSIOM_SHIFT)
    #define DF_UART_MISO_S_HSIOM_SEL_GPIO  (DF_UART_miso_s__0__HSIOM_GPIO)
    #define DF_UART_MISO_S_HSIOM_SEL_SPI   (DF_UART_miso_s__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_SLAVE_MISO_PIN) */

#if (DF_UART_SPI_MASTER_MISO_PIN)
    #define DF_UART_MISO_M_HSIOM_REG   (*(reg32 *) DF_UART_miso_m__0__HSIOM)
    #define DF_UART_MISO_M_HSIOM_PTR   ( (reg32 *) DF_UART_miso_m__0__HSIOM)
    
    #define DF_UART_MISO_M_HSIOM_MASK      (DF_UART_miso_m__0__HSIOM_MASK)
    #define DF_UART_MISO_M_HSIOM_POS       (DF_UART_miso_m__0__HSIOM_SHIFT)
    #define DF_UART_MISO_M_HSIOM_SEL_GPIO  (DF_UART_miso_m__0__HSIOM_GPIO)
    #define DF_UART_MISO_M_HSIOM_SEL_SPI   (DF_UART_miso_m__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_MASTER_MISO_PIN) */

#if (DF_UART_SPI_MASTER_MOSI_PIN)
    #define DF_UART_MOSI_M_HSIOM_REG   (*(reg32 *) DF_UART_mosi_m__0__HSIOM)
    #define DF_UART_MOSI_M_HSIOM_PTR   ( (reg32 *) DF_UART_mosi_m__0__HSIOM)
    
    #define DF_UART_MOSI_M_HSIOM_MASK      (DF_UART_mosi_m__0__HSIOM_MASK)
    #define DF_UART_MOSI_M_HSIOM_POS       (DF_UART_mosi_m__0__HSIOM_SHIFT)
    #define DF_UART_MOSI_M_HSIOM_SEL_GPIO  (DF_UART_mosi_m__0__HSIOM_GPIO)
    #define DF_UART_MOSI_M_HSIOM_SEL_SPI   (DF_UART_mosi_m__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_MASTER_MOSI_PIN) */

#if (DF_UART_SPI_MASTER_SCLK_PIN)
    #define DF_UART_SCLK_M_HSIOM_REG   (*(reg32 *) DF_UART_sclk_m__0__HSIOM)
    #define DF_UART_SCLK_M_HSIOM_PTR   ( (reg32 *) DF_UART_sclk_m__0__HSIOM)
    
    #define DF_UART_SCLK_M_HSIOM_MASK      (DF_UART_sclk_m__0__HSIOM_MASK)
    #define DF_UART_SCLK_M_HSIOM_POS       (DF_UART_sclk_m__0__HSIOM_SHIFT)
    #define DF_UART_SCLK_M_HSIOM_SEL_GPIO  (DF_UART_sclk_m__0__HSIOM_GPIO)
    #define DF_UART_SCLK_M_HSIOM_SEL_SPI   (DF_UART_sclk_m__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_MASTER_SCLK_PIN) */

#if (DF_UART_SPI_MASTER_SS0_PIN)
    #define DF_UART_SS0_M_HSIOM_REG    (*(reg32 *) DF_UART_ss0_m__0__HSIOM)
    #define DF_UART_SS0_M_HSIOM_PTR    ( (reg32 *) DF_UART_ss0_m__0__HSIOM)
    
    #define DF_UART_SS0_M_HSIOM_MASK       (DF_UART_ss0_m__0__HSIOM_MASK)
    #define DF_UART_SS0_M_HSIOM_POS        (DF_UART_ss0_m__0__HSIOM_SHIFT)
    #define DF_UART_SS0_M_HSIOM_SEL_GPIO   (DF_UART_ss0_m__0__HSIOM_GPIO)
    #define DF_UART_SS0_M_HSIOM_SEL_SPI    (DF_UART_ss0_m__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_MASTER_SS0_PIN) */

#if (DF_UART_SPI_MASTER_SS1_PIN)
    #define DF_UART_SS1_M_HSIOM_REG    (*(reg32 *) DF_UART_ss1_m__0__HSIOM)
    #define DF_UART_SS1_M_HSIOM_PTR    ( (reg32 *) DF_UART_ss1_m__0__HSIOM)
    
    #define DF_UART_SS1_M_HSIOM_MASK       (DF_UART_ss1_m__0__HSIOM_MASK)
    #define DF_UART_SS1_M_HSIOM_POS        (DF_UART_ss1_m__0__HSIOM_SHIFT)
    #define DF_UART_SS1_M_HSIOM_SEL_GPIO   (DF_UART_ss1_m__0__HSIOM_GPIO)
    #define DF_UART_SS1_M_HSIOM_SEL_SPI    (DF_UART_ss1_m__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_MASTER_SS1_PIN) */

#if (DF_UART_SPI_MASTER_SS2_PIN)
    #define DF_UART_SS2_M_HSIOM_REG    (*(reg32 *) DF_UART_ss2_m__0__HSIOM)
    #define DF_UART_SS2_M_HSIOM_PTR    ( (reg32 *) DF_UART_ss2_m__0__HSIOM)
    
    #define DF_UART_SS2_M_HSIOM_MASK       (DF_UART_ss2_m__0__HSIOM_MASK)
    #define DF_UART_SS2_M_HSIOM_POS        (DF_UART_ss2_m__0__HSIOM_SHIFT)
    #define DF_UART_SS2_M_HSIOM_SEL_GPIO   (DF_UART_ss2_m__0__HSIOM_GPIO)
    #define DF_UART_SS2_M_HSIOM_SEL_SPI    (DF_UART_ss2_m__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_MASTER_SS2_PIN) */

#if (DF_UART_SPI_MASTER_SS3_PIN)
    #define DF_UART_SS3_M_HSIOM_REG    (*(reg32 *) DF_UART_ss3_m__0__HSIOM)
    #define DF_UART_SS3_M_HSIOM_PTR    ( (reg32 *) DF_UART_ss3_m__0__HSIOM)
    
    #define DF_UART_SS3_M_HSIOM_MASK      (DF_UART_ss3_m__0__HSIOM_MASK)
    #define DF_UART_SS3_M_HSIOM_POS       (DF_UART_ss3_m__0__HSIOM_SHIFT)
    #define DF_UART_SS3_M_HSIOM_SEL_GPIO  (DF_UART_ss3_m__0__HSIOM_GPIO)
    #define DF_UART_SS3_M_HSIOM_SEL_SPI   (DF_UART_ss3_m__0__HSIOM_SPI)
#endif /* (DF_UART_SPI_MASTER_SS3_PIN) */

#if (DF_UART_UART_RX_PIN)
    #define DF_UART_RX_HSIOM_REG   (*(reg32 *) DF_UART_rx__0__HSIOM)
    #define DF_UART_RX_HSIOM_PTR   ( (reg32 *) DF_UART_rx__0__HSIOM)
    
    #define DF_UART_RX_HSIOM_MASK      (DF_UART_rx__0__HSIOM_MASK)
    #define DF_UART_RX_HSIOM_POS       (DF_UART_rx__0__HSIOM_SHIFT)
    #define DF_UART_RX_HSIOM_SEL_GPIO  (DF_UART_rx__0__HSIOM_GPIO)
    #define DF_UART_RX_HSIOM_SEL_UART  (DF_UART_rx__0__HSIOM_UART)
#endif /* (DF_UART_UART_RX_PIN) */

#if (DF_UART_UART_RX_WAKE_PIN)
    #define DF_UART_RX_WAKE_HSIOM_REG   (*(reg32 *) DF_UART_rx_wake__0__HSIOM)
    #define DF_UART_RX_WAKE_HSIOM_PTR   ( (reg32 *) DF_UART_rx_wake__0__HSIOM)
    
    #define DF_UART_RX_WAKE_HSIOM_MASK      (DF_UART_rx_wake__0__HSIOM_MASK)
    #define DF_UART_RX_WAKE_HSIOM_POS       (DF_UART_rx_wake__0__HSIOM_SHIFT)
    #define DF_UART_RX_WAKE_HSIOM_SEL_GPIO  (DF_UART_rx_wake__0__HSIOM_GPIO)
    #define DF_UART_RX_WAKE_HSIOM_SEL_UART  (DF_UART_rx_wake__0__HSIOM_UART)
#endif /* (DF_UART_UART_WAKE_RX_PIN) */

#if (DF_UART_UART_CTS_PIN)
    #define DF_UART_CTS_HSIOM_REG   (*(reg32 *) DF_UART_cts__0__HSIOM)
    #define DF_UART_CTS_HSIOM_PTR   ( (reg32 *) DF_UART_cts__0__HSIOM)
    
    #define DF_UART_CTS_HSIOM_MASK      (DF_UART_cts__0__HSIOM_MASK)
    #define DF_UART_CTS_HSIOM_POS       (DF_UART_cts__0__HSIOM_SHIFT)
    #define DF_UART_CTS_HSIOM_SEL_GPIO  (DF_UART_cts__0__HSIOM_GPIO)
    #define DF_UART_CTS_HSIOM_SEL_UART  (DF_UART_cts__0__HSIOM_UART)
#endif /* (DF_UART_UART_CTS_PIN) */

#if (DF_UART_UART_TX_PIN)
    #define DF_UART_TX_HSIOM_REG   (*(reg32 *) DF_UART_tx__0__HSIOM)
    #define DF_UART_TX_HSIOM_PTR   ( (reg32 *) DF_UART_tx__0__HSIOM)
    
    #define DF_UART_TX_HSIOM_MASK      (DF_UART_tx__0__HSIOM_MASK)
    #define DF_UART_TX_HSIOM_POS       (DF_UART_tx__0__HSIOM_SHIFT)
    #define DF_UART_TX_HSIOM_SEL_GPIO  (DF_UART_tx__0__HSIOM_GPIO)
    #define DF_UART_TX_HSIOM_SEL_UART  (DF_UART_tx__0__HSIOM_UART)
#endif /* (DF_UART_UART_TX_PIN) */

#if (DF_UART_UART_RX_TX_PIN)
    #define DF_UART_RX_TX_HSIOM_REG   (*(reg32 *) DF_UART_rx_tx__0__HSIOM)
    #define DF_UART_RX_TX_HSIOM_PTR   ( (reg32 *) DF_UART_rx_tx__0__HSIOM)
    
    #define DF_UART_RX_TX_HSIOM_MASK      (DF_UART_rx_tx__0__HSIOM_MASK)
    #define DF_UART_RX_TX_HSIOM_POS       (DF_UART_rx_tx__0__HSIOM_SHIFT)
    #define DF_UART_RX_TX_HSIOM_SEL_GPIO  (DF_UART_rx_tx__0__HSIOM_GPIO)
    #define DF_UART_RX_TX_HSIOM_SEL_UART  (DF_UART_rx_tx__0__HSIOM_UART)
#endif /* (DF_UART_UART_RX_TX_PIN) */

#if (DF_UART_UART_RTS_PIN)
    #define DF_UART_RTS_HSIOM_REG      (*(reg32 *) DF_UART_rts__0__HSIOM)
    #define DF_UART_RTS_HSIOM_PTR      ( (reg32 *) DF_UART_rts__0__HSIOM)
    
    #define DF_UART_RTS_HSIOM_MASK     (DF_UART_rts__0__HSIOM_MASK)
    #define DF_UART_RTS_HSIOM_POS      (DF_UART_rts__0__HSIOM_SHIFT)    
    #define DF_UART_RTS_HSIOM_SEL_GPIO (DF_UART_rts__0__HSIOM_GPIO)
    #define DF_UART_RTS_HSIOM_SEL_UART (DF_UART_rts__0__HSIOM_UART)    
#endif /* (DF_UART_UART_RTS_PIN) */


/***************************************
*        Registers Constants
***************************************/

/* HSIOM switch values. */ 
#define DF_UART_HSIOM_DEF_SEL      (0x00u)
#define DF_UART_HSIOM_GPIO_SEL     (0x00u)
/* The HSIOM values provided below are valid only for DF_UART_CY_SCBIP_V0 
* and DF_UART_CY_SCBIP_V1. It is not recommended to use them for 
* DF_UART_CY_SCBIP_V2. Use pin name specific HSIOM constants provided 
* above instead for any SCB IP block version.
*/
#define DF_UART_HSIOM_UART_SEL     (0x09u)
#define DF_UART_HSIOM_I2C_SEL      (0x0Eu)
#define DF_UART_HSIOM_SPI_SEL      (0x0Fu)

/* Pins settings index. */
#define DF_UART_RX_WAKE_SDA_MOSI_PIN_INDEX   (0u)
#define DF_UART_RX_SDA_MOSI_PIN_INDEX       (0u)
#define DF_UART_TX_SCL_MISO_PIN_INDEX       (1u)
#define DF_UART_CTS_SCLK_PIN_INDEX       (2u)
#define DF_UART_RTS_SS0_PIN_INDEX       (3u)
#define DF_UART_SS1_PIN_INDEX                  (4u)
#define DF_UART_SS2_PIN_INDEX                  (5u)
#define DF_UART_SS3_PIN_INDEX                  (6u)

/* Pins settings mask. */
#define DF_UART_RX_WAKE_SDA_MOSI_PIN_MASK ((uint32) 0x01u << DF_UART_RX_WAKE_SDA_MOSI_PIN_INDEX)
#define DF_UART_RX_SDA_MOSI_PIN_MASK     ((uint32) 0x01u << DF_UART_RX_SDA_MOSI_PIN_INDEX)
#define DF_UART_TX_SCL_MISO_PIN_MASK     ((uint32) 0x01u << DF_UART_TX_SCL_MISO_PIN_INDEX)
#define DF_UART_CTS_SCLK_PIN_MASK     ((uint32) 0x01u << DF_UART_CTS_SCLK_PIN_INDEX)
#define DF_UART_RTS_SS0_PIN_MASK     ((uint32) 0x01u << DF_UART_RTS_SS0_PIN_INDEX)
#define DF_UART_SS1_PIN_MASK                ((uint32) 0x01u << DF_UART_SS1_PIN_INDEX)
#define DF_UART_SS2_PIN_MASK                ((uint32) 0x01u << DF_UART_SS2_PIN_INDEX)
#define DF_UART_SS3_PIN_MASK                ((uint32) 0x01u << DF_UART_SS3_PIN_INDEX)

/* Pin interrupt constants. */
#define DF_UART_INTCFG_TYPE_MASK           (0x03u)
#define DF_UART_INTCFG_TYPE_FALLING_EDGE   (0x02u)

/* Pin Drive Mode constants. */
#define DF_UART_PIN_DM_ALG_HIZ  (0u)
#define DF_UART_PIN_DM_DIG_HIZ  (1u)
#define DF_UART_PIN_DM_OD_LO    (4u)
#define DF_UART_PIN_DM_STRONG   (6u)


/***************************************
*          Macro Definitions
***************************************/

/* Return drive mode of the pin */
#define DF_UART_DM_MASK    (0x7u)
#define DF_UART_DM_SIZE    (3u)
#define DF_UART_GET_P4_PIN_DM(reg, pos) \
    ( ((reg) & (uint32) ((uint32) DF_UART_DM_MASK << (DF_UART_DM_SIZE * (pos)))) >> \
                                                              (DF_UART_DM_SIZE * (pos)) )

#if (DF_UART_TX_SCL_MISO_PIN)
    #define DF_UART_CHECK_TX_SCL_MISO_PIN_USED \
                (DF_UART_PIN_DM_ALG_HIZ != \
                    DF_UART_GET_P4_PIN_DM(DF_UART_uart_tx_i2c_scl_spi_miso_PC, \
                                                   DF_UART_uart_tx_i2c_scl_spi_miso_SHIFT))
#endif /* (DF_UART_TX_SCL_MISO_PIN) */

#if (DF_UART_RTS_SS0_PIN)
    #define DF_UART_CHECK_RTS_SS0_PIN_USED \
                (DF_UART_PIN_DM_ALG_HIZ != \
                    DF_UART_GET_P4_PIN_DM(DF_UART_uart_rts_spi_ss0_PC, \
                                                   DF_UART_uart_rts_spi_ss0_SHIFT))
#endif /* (DF_UART_RTS_SS0_PIN) */

/* Set bits-mask in register */
#define DF_UART_SET_REGISTER_BITS(reg, mask, pos, mode) \
                    do                                           \
                    {                                            \
                        (reg) = (((reg) & ((uint32) ~(uint32) (mask))) | ((uint32) ((uint32) (mode) << (pos)))); \
                    }while(0)

/* Set bit in the register */
#define DF_UART_SET_REGISTER_BIT(reg, mask, val) \
                    ((val) ? ((reg) |= (mask)) : ((reg) &= ((uint32) ~((uint32) (mask)))))

#define DF_UART_SET_HSIOM_SEL(reg, mask, pos, sel) DF_UART_SET_REGISTER_BITS(reg, mask, pos, sel)
#define DF_UART_SET_INCFG_TYPE(reg, mask, pos, intType) \
                                                        DF_UART_SET_REGISTER_BITS(reg, mask, pos, intType)
#define DF_UART_SET_INP_DIS(reg, mask, val) DF_UART_SET_REGISTER_BIT(reg, mask, val)

/* DF_UART_SET_I2C_SCL_DR(val) - Sets I2C SCL DR register.
*  DF_UART_SET_I2C_SCL_HSIOM_SEL(sel) - Sets I2C SCL HSIOM settings.
*/
/* SCB I2C: scl signal */
#if (DF_UART_CY_SCBIP_V0)
#if (DF_UART_I2C_PINS)
    #define DF_UART_SET_I2C_SCL_DR(val) DF_UART_scl_Write(val)

    #define DF_UART_SET_I2C_SCL_HSIOM_SEL(sel) \
                          DF_UART_SET_HSIOM_SEL(DF_UART_SCL_HSIOM_REG,  \
                                                         DF_UART_SCL_HSIOM_MASK, \
                                                         DF_UART_SCL_HSIOM_POS,  \
                                                         (sel))
    #define DF_UART_WAIT_SCL_SET_HIGH  (0u == DF_UART_scl_Read())

/* Unconfigured SCB: scl signal */
#elif (DF_UART_RX_WAKE_SDA_MOSI_PIN)
    #define DF_UART_SET_I2C_SCL_DR(val) \
                            DF_UART_uart_rx_wake_i2c_sda_spi_mosi_Write(val)

    #define DF_UART_SET_I2C_SCL_HSIOM_SEL(sel) \
                    DF_UART_SET_HSIOM_SEL(DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG,  \
                                                   DF_UART_RX_WAKE_SDA_MOSI_HSIOM_MASK, \
                                                   DF_UART_RX_WAKE_SDA_MOSI_HSIOM_POS,  \
                                                   (sel))

    #define DF_UART_WAIT_SCL_SET_HIGH  (0u == DF_UART_uart_rx_wake_i2c_sda_spi_mosi_Read())

#elif (DF_UART_RX_SDA_MOSI_PIN)
    #define DF_UART_SET_I2C_SCL_DR(val) \
                            DF_UART_uart_rx_i2c_sda_spi_mosi_Write(val)


    #define DF_UART_SET_I2C_SCL_HSIOM_SEL(sel) \
                            DF_UART_SET_HSIOM_SEL(DF_UART_RX_SDA_MOSI_HSIOM_REG,  \
                                                           DF_UART_RX_SDA_MOSI_HSIOM_MASK, \
                                                           DF_UART_RX_SDA_MOSI_HSIOM_POS,  \
                                                           (sel))

    #define DF_UART_WAIT_SCL_SET_HIGH  (0u == DF_UART_uart_rx_i2c_sda_spi_mosi_Read())

#else
    #define DF_UART_SET_I2C_SCL_DR(val)        do{ /* Does nothing */ }while(0)
    #define DF_UART_SET_I2C_SCL_HSIOM_SEL(sel) do{ /* Does nothing */ }while(0)

    #define DF_UART_WAIT_SCL_SET_HIGH  (0u)
#endif /* (DF_UART_I2C_PINS) */

/* SCB I2C: sda signal */
#if (DF_UART_I2C_PINS)
    #define DF_UART_WAIT_SDA_SET_HIGH  (0u == DF_UART_sda_Read())
/* Unconfigured SCB: sda signal */
#elif (DF_UART_TX_SCL_MISO_PIN)
    #define DF_UART_WAIT_SDA_SET_HIGH  (0u == DF_UART_uart_tx_i2c_scl_spi_miso_Read())
#else
    #define DF_UART_WAIT_SDA_SET_HIGH  (0u)
#endif /* (DF_UART_MOSI_SCL_RX_PIN) */
#endif /* (DF_UART_CY_SCBIP_V0) */

/* Clear UART wakeup source */
#if (DF_UART_RX_SDA_MOSI_PIN)
    #define DF_UART_CLEAR_UART_RX_WAKE_INTR        do{ /* Does nothing */ }while(0)
    
#elif (DF_UART_RX_WAKE_SDA_MOSI_PIN)
    #define DF_UART_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) DF_UART_uart_rx_wake_i2c_sda_spi_mosi_ClearInterrupt(); \
            }while(0)

#elif(DF_UART_UART_RX_WAKE_PIN)
    #define DF_UART_CLEAR_UART_RX_WAKE_INTR \
            do{                                      \
                (void) DF_UART_rx_wake_ClearInterrupt(); \
            }while(0)
#else
#endif /* (DF_UART_RX_SDA_MOSI_PIN) */


/***************************************
* The following code is DEPRECATED and
* must not be used.
***************************************/

/* Unconfigured pins */
#define DF_UART_REMOVE_MOSI_SCL_RX_WAKE_PIN    DF_UART_REMOVE_RX_WAKE_SDA_MOSI_PIN
#define DF_UART_REMOVE_MOSI_SCL_RX_PIN         DF_UART_REMOVE_RX_SDA_MOSI_PIN
#define DF_UART_REMOVE_MISO_SDA_TX_PIN         DF_UART_REMOVE_TX_SCL_MISO_PIN
#ifndef DF_UART_REMOVE_SCLK_PIN
#define DF_UART_REMOVE_SCLK_PIN                DF_UART_REMOVE_CTS_SCLK_PIN
#endif /* DF_UART_REMOVE_SCLK_PIN */
#ifndef DF_UART_REMOVE_SS0_PIN
#define DF_UART_REMOVE_SS0_PIN                 DF_UART_REMOVE_RTS_SS0_PIN
#endif /* DF_UART_REMOVE_SS0_PIN */

/* Unconfigured pins */
#define DF_UART_MOSI_SCL_RX_WAKE_PIN   DF_UART_RX_WAKE_SDA_MOSI_PIN
#define DF_UART_MOSI_SCL_RX_PIN        DF_UART_RX_SDA_MOSI_PIN
#define DF_UART_MISO_SDA_TX_PIN        DF_UART_TX_SCL_MISO_PIN
#ifndef DF_UART_SCLK_PIN
#define DF_UART_SCLK_PIN               DF_UART_CTS_SCLK_PIN
#endif /* DF_UART_SCLK_PIN */
#ifndef DF_UART_SS0_PIN
#define DF_UART_SS0_PIN                DF_UART_RTS_SS0_PIN
#endif /* DF_UART_SS0_PIN */

#if (DF_UART_MOSI_SCL_RX_WAKE_PIN)
    #define DF_UART_MOSI_SCL_RX_WAKE_HSIOM_REG     DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define DF_UART_MOSI_SCL_RX_WAKE_HSIOM_PTR     DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define DF_UART_MOSI_SCL_RX_WAKE_HSIOM_MASK    DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define DF_UART_MOSI_SCL_RX_WAKE_HSIOM_POS     DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG

    #define DF_UART_MOSI_SCL_RX_WAKE_INTCFG_REG    DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define DF_UART_MOSI_SCL_RX_WAKE_INTCFG_PTR    DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG

    #define DF_UART_MOSI_SCL_RX_WAKE_INTCFG_TYPE_POS   DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG
    #define DF_UART_MOSI_SCL_RX_WAKE_INTCFG_TYPE_MASK  DF_UART_RX_WAKE_SDA_MOSI_HSIOM_REG
#endif /* (DF_UART_RX_WAKE_SDA_MOSI_PIN) */

#if (DF_UART_MOSI_SCL_RX_PIN)
    #define DF_UART_MOSI_SCL_RX_HSIOM_REG      DF_UART_RX_SDA_MOSI_HSIOM_REG
    #define DF_UART_MOSI_SCL_RX_HSIOM_PTR      DF_UART_RX_SDA_MOSI_HSIOM_PTR
    #define DF_UART_MOSI_SCL_RX_HSIOM_MASK     DF_UART_RX_SDA_MOSI_HSIOM_MASK
    #define DF_UART_MOSI_SCL_RX_HSIOM_POS      DF_UART_RX_SDA_MOSI_HSIOM_POS
#endif /* (DF_UART_MOSI_SCL_RX_PIN) */

#if (DF_UART_MISO_SDA_TX_PIN)
    #define DF_UART_MISO_SDA_TX_HSIOM_REG      DF_UART_TX_SCL_MISO_HSIOM_REG
    #define DF_UART_MISO_SDA_TX_HSIOM_PTR      DF_UART_TX_SCL_MISO_HSIOM_REG
    #define DF_UART_MISO_SDA_TX_HSIOM_MASK     DF_UART_TX_SCL_MISO_HSIOM_REG
    #define DF_UART_MISO_SDA_TX_HSIOM_POS      DF_UART_TX_SCL_MISO_HSIOM_REG
#endif /* (DF_UART_MISO_SDA_TX_PIN_PIN) */

#if (DF_UART_SCLK_PIN)
    #ifndef DF_UART_SCLK_HSIOM_REG
    #define DF_UART_SCLK_HSIOM_REG     DF_UART_CTS_SCLK_HSIOM_REG
    #define DF_UART_SCLK_HSIOM_PTR     DF_UART_CTS_SCLK_HSIOM_PTR
    #define DF_UART_SCLK_HSIOM_MASK    DF_UART_CTS_SCLK_HSIOM_MASK
    #define DF_UART_SCLK_HSIOM_POS     DF_UART_CTS_SCLK_HSIOM_POS
    #endif /* DF_UART_SCLK_HSIOM_REG */
#endif /* (DF_UART_SCLK_PIN) */

#if (DF_UART_SS0_PIN)
    #ifndef DF_UART_SS0_HSIOM_REG
    #define DF_UART_SS0_HSIOM_REG      DF_UART_RTS_SS0_HSIOM_REG
    #define DF_UART_SS0_HSIOM_PTR      DF_UART_RTS_SS0_HSIOM_PTR
    #define DF_UART_SS0_HSIOM_MASK     DF_UART_RTS_SS0_HSIOM_MASK
    #define DF_UART_SS0_HSIOM_POS      DF_UART_RTS_SS0_HSIOM_POS
    #endif /* DF_UART_SS0_HSIOM_REG */
#endif /* (DF_UART_SS0_PIN) */

#define DF_UART_MOSI_SCL_RX_WAKE_PIN_INDEX DF_UART_RX_WAKE_SDA_MOSI_PIN_INDEX
#define DF_UART_MOSI_SCL_RX_PIN_INDEX      DF_UART_RX_SDA_MOSI_PIN_INDEX
#define DF_UART_MISO_SDA_TX_PIN_INDEX      DF_UART_TX_SCL_MISO_PIN_INDEX
#ifndef DF_UART_SCLK_PIN_INDEX
#define DF_UART_SCLK_PIN_INDEX             DF_UART_CTS_SCLK_PIN_INDEX
#endif /* DF_UART_SCLK_PIN_INDEX */
#ifndef DF_UART_SS0_PIN_INDEX
#define DF_UART_SS0_PIN_INDEX              DF_UART_RTS_SS0_PIN_INDEX
#endif /* DF_UART_SS0_PIN_INDEX */

#define DF_UART_MOSI_SCL_RX_WAKE_PIN_MASK DF_UART_RX_WAKE_SDA_MOSI_PIN_MASK
#define DF_UART_MOSI_SCL_RX_PIN_MASK      DF_UART_RX_SDA_MOSI_PIN_MASK
#define DF_UART_MISO_SDA_TX_PIN_MASK      DF_UART_TX_SCL_MISO_PIN_MASK
#ifndef DF_UART_SCLK_PIN_MASK
#define DF_UART_SCLK_PIN_MASK             DF_UART_CTS_SCLK_PIN_MASK
#endif /* DF_UART_SCLK_PIN_MASK */
#ifndef DF_UART_SS0_PIN_MASK
#define DF_UART_SS0_PIN_MASK              DF_UART_RTS_SS0_PIN_MASK
#endif /* DF_UART_SS0_PIN_MASK */

#endif /* (CY_SCB_PINS_DF_UART_H) */


/* [] END OF FILE */
