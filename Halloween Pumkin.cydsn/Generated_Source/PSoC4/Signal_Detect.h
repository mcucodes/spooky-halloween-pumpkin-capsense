/*******************************************************************************
* File Name: Signal_Detect.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Signal_Detect_H) /* Pins Signal_Detect_H */
#define CY_PINS_Signal_Detect_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Signal_Detect_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} Signal_Detect_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   Signal_Detect_Read(void);
void    Signal_Detect_Write(uint8 value);
uint8   Signal_Detect_ReadDataReg(void);
#if defined(Signal_Detect__PC) || (CY_PSOC4_4200L) 
    void    Signal_Detect_SetDriveMode(uint8 mode);
#endif
void    Signal_Detect_SetInterruptMode(uint16 position, uint16 mode);
uint8   Signal_Detect_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void Signal_Detect_Sleep(void); 
void Signal_Detect_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(Signal_Detect__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define Signal_Detect_DRIVE_MODE_BITS        (3)
    #define Signal_Detect_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Signal_Detect_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the Signal_Detect_SetDriveMode() function.
         *  @{
         */
        #define Signal_Detect_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define Signal_Detect_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define Signal_Detect_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define Signal_Detect_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define Signal_Detect_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define Signal_Detect_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define Signal_Detect_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define Signal_Detect_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define Signal_Detect_MASK               Signal_Detect__MASK
#define Signal_Detect_SHIFT              Signal_Detect__SHIFT
#define Signal_Detect_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Signal_Detect_SetInterruptMode() function.
     *  @{
     */
        #define Signal_Detect_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define Signal_Detect_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define Signal_Detect_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define Signal_Detect_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(Signal_Detect__SIO)
    #define Signal_Detect_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(Signal_Detect__PC) && (CY_PSOC4_4200L)
    #define Signal_Detect_USBIO_ENABLE               ((uint32)0x80000000u)
    #define Signal_Detect_USBIO_DISABLE              ((uint32)(~Signal_Detect_USBIO_ENABLE))
    #define Signal_Detect_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define Signal_Detect_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define Signal_Detect_USBIO_ENTER_SLEEP          ((uint32)((1u << Signal_Detect_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << Signal_Detect_USBIO_SUSPEND_DEL_SHIFT)))
    #define Signal_Detect_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << Signal_Detect_USBIO_SUSPEND_SHIFT)))
    #define Signal_Detect_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << Signal_Detect_USBIO_SUSPEND_DEL_SHIFT)))
    #define Signal_Detect_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(Signal_Detect__PC)
    /* Port Configuration */
    #define Signal_Detect_PC                 (* (reg32 *) Signal_Detect__PC)
#endif
/* Pin State */
#define Signal_Detect_PS                     (* (reg32 *) Signal_Detect__PS)
/* Data Register */
#define Signal_Detect_DR                     (* (reg32 *) Signal_Detect__DR)
/* Input Buffer Disable Override */
#define Signal_Detect_INP_DIS                (* (reg32 *) Signal_Detect__PC2)

/* Interrupt configuration Registers */
#define Signal_Detect_INTCFG                 (* (reg32 *) Signal_Detect__INTCFG)
#define Signal_Detect_INTSTAT                (* (reg32 *) Signal_Detect__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define Signal_Detect_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(Signal_Detect__SIO)
    #define Signal_Detect_SIO_REG            (* (reg32 *) Signal_Detect__SIO)
#endif /* (Signal_Detect__SIO_CFG) */

/* USBIO registers */
#if !defined(Signal_Detect__PC) && (CY_PSOC4_4200L)
    #define Signal_Detect_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define Signal_Detect_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define Signal_Detect_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define Signal_Detect_DRIVE_MODE_SHIFT       (0x00u)
#define Signal_Detect_DRIVE_MODE_MASK        (0x07u << Signal_Detect_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins Signal_Detect_H */


/* [] END OF FILE */
