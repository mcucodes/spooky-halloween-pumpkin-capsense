/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "UART.h"
#include "dfplay.h"

char8 DFTXBuffer[16];



static  void UART_UartWriteBytes(const char8 data[], char8 length)
{
    uint32 bufIndex;

    bufIndex = 0u;

    /* Blocks the control flow until all data has been sent */
    while(bufIndex < length)
    {
        DF_UART_UartPutChar((uint32)data[bufIndex]);
        bufIndex++;
    }
}

static void df_writeframe(char8 cmds[]){
    
    char8 length =6;
    int16 checksum =0;
    
    DFTXBuffer[0] = 0x7E; //Start CMD
    DFTXBuffer[1] = 0xFF; //Version CMD
    DFTXBuffer[2] = length & 0xFF; //Length
    DFTXBuffer[3] = cmds[0];
    DFTXBuffer[4] = 0x0;  //Feedback YES?NO
    DFTXBuffer[5] = cmds[1];  //Hbyte
    DFTXBuffer[6] = cmds[2];  //LByte
    
    checksum = 0 - (DFTXBuffer[1]+DFTXBuffer[2]+DFTXBuffer[3]+DFTXBuffer[4]+
                    DFTXBuffer[5]+DFTXBuffer[6]);
    
    DFTXBuffer[7] = (checksum>>8 & 0xFF);
    DFTXBuffer[8] = (checksum & 0xFF);
    DFTXBuffer[9] = 0xEF; //Stop CMD
    
    UART_UartWriteBytes(DFTXBuffer,10);
    
    
}


void DF_Play(uint16 track){
    
    char8 cmd[3]={DF_PLAY,0,0};
 
    cmd[1]= track>>8 & 0xFF;
    cmd[2]= track & 0xFF;
    
    df_writeframe(cmd);
    
}

void DF_PlaySel(uint8 folder,uint8 track){
    
    char8 cmd[3]={DF_SEPLY,0,0};
 
    cmd[1]= folder & 0xFF;
    cmd[2]= track & 0xFF;
    
    df_writeframe(cmd);
    
}

void DF_DeviceVolume(uint8 level){
    
    char8 cmd[3]={DF_SEVOL,0,0};
 
    cmd[2]= level & 0xFF;
    
    df_writeframe(cmd);
    
}



void DF_DeviceSel(uint8 device){
    
    char8 cmd[3]={DF_SEDEV,0,0};
 
    cmd[2]= device & 0xFF;
    
    df_writeframe(cmd);
    
}


void DF_DeviceReset(){
    
    char8 cmd[3]={DF_RESET,0,0};
    
    df_writeframe(cmd);
    
}

void DF_Init(){
    
    
    
    
    
    
}








/* [] END OF FILE */
